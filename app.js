const express = require('express')
const app = express()
const port = 8080

app.get('/', (req,res) => {
    res.send('first node js cicd proj')
})

app.listen(port, () => {
    console.log('appln at http://localhost:$(port)')
})
